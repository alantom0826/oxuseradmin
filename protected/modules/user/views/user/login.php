<div class="row">
    <div  class="small-6 small-centered columns">
		
<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
//~ $this->breadcrumbs=array(
	//~ UserModule::t("Login"),
//~ );
?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jcap.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/md5.js"></script>
<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/ossiilogo.png" alt='test' >
<P> </P>
<h2><?php //echo UserModule::t("OSSII線上測驗系統"); ?></h2>
<h2><?php echo UserModule::t("OSSII會員管理系統"); ?></h2>

<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; ?>

<p><?php echo UserModule::t("Please input your account and password :"); ?></p>

 

		
<div class="form"  onsubmit="return jcap();">
<?php echo CHtml::beginForm(); ?>
	<!--
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	-->
	<?php echo CHtml::errorSummary($model); ?>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'username'); ?>
		<?php echo CHtml::activeTextField($model,'username') ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'password'); ?>
		<?php echo CHtml::activePasswordField($model,'password') ?>
	</div>
	
<!--
	<div class="row">
		<p class="hint">
		<?php //echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?> | <?php //echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
		</p>
	</div>
-->
	
	<div class="row rememberMe">
		<?php //echo CHtml::activeCheckBox($model,'rememberMe'); ?>
		<?php //echo CHtml::activeLabelEx($model,'rememberMe'); ?>
	</div>
    
	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php CHtml::activeLabelEx($model,'verifyCode'); ?>
		<div>
		<?php //$this->widget('CCaptcha'); ?>
        <?php $this->widget ( 'CCaptcha', array (
                  'showRefreshButton' => true,
                  'clickableImage' => true,
                  'buttonType' => 'link', 'buttonLabel' => UserModule::t("Change Picture"),'buttonOptions'=>array(
                  "class"=>"refreshLabel","style"=>"margin:10px 5px 0"),
                  'imageOptions' => array (
                  'alt' => '點擊換圖',
                  'title' => '點擊換圖',
                  'style' => 'vertical-align:middle;cursor:pointer;border:1px #dadada solid;height:100px;float:left' ) ) );?>
		</div>
		<?php CHtml::errorSummary($model,'verifyCode'); ?>
	</div>
    <?php echo CHtml::activeTextField($model,'verifyCode'); ?>
	<?php endif; ?>

<!--
    <script type="text/javascript">sjcap();</script>
-->

	<div class="row submit">
		<?php //echo CHtml::submitButton(UserModule::t("Login")); ?>
        <input class="button radius" type="submit" value="<?php echo UserModule::t("Login")?>">
	</div>
	
<?php echo CHtml::endForm(); ?>
</div><!-- form -->

<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>


</div>
	</div>
