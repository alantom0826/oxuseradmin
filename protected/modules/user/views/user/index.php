<?php
$this->breadcrumbs=array(
	UserModule::t("Users"),
);
if(UserModule::isAdmin()) {
	$this->layout='//layouts/column2';
	$this->menu=array(
	    array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')),
	    //array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
	);
}
?>

<h1><?php echo UserModule::t("List User"); ?></h1>

<?php 
    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'columns'=>array(
            array(
                'name' => UserModule::t('Account'),
                'type'=>'raw',
                'value' => 'CHtml::link(CHtml::encode($data->username),array("admin/view","id"=>$data->id))',
            ),
            'name',
            'email',
            //'class_info',
            array(
                'name' => '教育程度',
                'value' => 'json_decode($data->class_info)[0][0]',
            ),
            array(
                'name' => '學校(分校)',
                'value' => 'json_decode($data->class_info)[0][1]',
            ),
            array(
                'name' => '學年',
                'value' => 'json_decode($data->class_info)[0][2]',
            ),
            array(
                'name' => '學期',
                'value' => 'json_decode($data->class_info)[0][3]',
            ),
            array(
                'name' => '年級',
                'value' => 'json_decode($data->class_info)[0][4]',
            ),
            array(
                'name' => '班級',
                'value' => 'json_decode($data->class_info)[0][5]',
            ),
            'create_at',
            'lastvisit_at',
        ),
    ));
    
//echo '<pre />';
//print_r($dataProvider->getData()[2]->attributes['class_info']);

?>

