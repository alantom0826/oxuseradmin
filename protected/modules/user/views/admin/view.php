<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	$model->username,
);


$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create')),
    array('label'=>UserModule::t('Update User'), 'url'=>array('update','id'=>$model->id)),
    array('label'=>UserModule::t('Delete User'), 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this item?'))),
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
    //array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
);
?>
<h1><?php //echo UserModule::t('View User').' "'.$model->username.'"'; ?></h1>
<h1><?php echo UserModule::t('View User').' "'.$model->name.'"'; ?></h1>
<?php
$class_infoary = json_decode($model->class_info);

	$attributes = array(
		'id',
		'username',
        'name',
	);
	
	/*$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
	if ($profileFields) {
		foreach($profileFields as $field) {
			array_push($attributes,array(
					'label' => UserModule::t($field->title),
					'name' => $field->varname,
					'type'=>'raw',
					'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),
				));
		}
	}*/
	
	array_push($attributes,
        //'class_info',
		//'password',
		'email',
		//'activkey',
		'create_at',
		'lastvisit_at',
		/*array(
			'name' => 'superuser',
			'value' => User::itemAlias("AdminStatus",$model->superuser),
		),*/
        array(
			'name' => '教育程度',
			'value' => $class_infoary[0][0],
		),
        array(
			'name' => '學校(分校)',
			'value' => $class_infoary[0][1],
		),
        array(
			'name' => '學年',
			'value' => $class_infoary[0][2],
		),
        array(
			'name' => '學期',
			'value' => $class_infoary[0][3],
		),
        array(
			'name' => '年級',
			'value' => $class_infoary[0][4],
		),
        array(
			'name' => '班級',
			'value' => $class_infoary[0][5],
		),
		array(
			'name' => 'status',
			'value' => User::itemAlias("UserStatus",$model->status),
		)
	);

	$this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>$attributes,
	));

?>
