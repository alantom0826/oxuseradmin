<?php

class OxexamModule extends CWebModule
{
    
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'oxexam.models.*',
			'oxexam.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
    
    public function lang($language){
        switch ($language){
            case 'zh_tw':
                return '繁體中文';
            break;
            case 'zh_cn':
                return '簡體中文';
            break;
            case 'en_us':
                return '英文';
            break;
        }
    }
    
    public function loadconf($arg){
        $fp = fopen($_SERVER['DOCUMENT_ROOT']. "/oxexam_online/oxexam.conf","r");
        $json = fread($fp,filesize($_SERVER['DOCUMENT_ROOT']. "/oxexam_online/oxexam.conf"));
        $file = json_decode($json);
        $result = $file->$arg;
        
        return $result;
    }
    
    public function color_for_reading($answertmp, $papertmp, $k, $q, $a){
        if (isset($answertmp[$k][$q][$a])){
            if (!strcasecmp(substr($answertmp[$k][$q][$a], 1), $papertmp[$k][$q][$a])){
                return "269926";
            }else{
                if (substr($answertmp[$k][$q][$a], 1) == ""){return "5d6163";}
                return "BA2F39";
            }
        }else{
            return "5d6163";
        }
    }

    public function color_for($answertmp, $papertmp, $k, $q){
        for ($a=0; $a < count($papertmp[$k][$q]); $a++){
            if (isset($answertmp[$k][$q][$a])){
                if (!strcasecmp(substr($answertmp[$k][$q][$a], 1), $papertmp[$k][$q][$a])){
                    return "269926";
                }else{
                    if (substr($answertmp[$k][$q][$a], 1) == ""){return "5d6163";}
                    return "BA2F39";
                }
            }else{
                return "5d6163";
            }
        }
    }
    
    public function ans_for_reading($answertmp, $papertmp, $k, $q, $a){
        if (isset($answertmp[$k][$q][$a])){
            echo substr($answertmp[$k][$q][$a], 1) == ""?"<div style=\"color:blue\">" ."未作答!!". "</div>":"你的答案為 ".substr($answertmp[$k][$q][$a], 1)."<br />";
            echo "正確答案為 " .$papertmp[$k][$q][$a]. "<br />";
            if (!strcasecmp(substr($answertmp[$k][$q][$a], 1), $papertmp[$k][$q][$a])){
                echo "<div style=\"color:green\">" ."答對!!". "</div>";
            }else{
                echo substr($answertmp[$k][$q][$a], 1) == ""?"":"<div style=\"color:red\">" ."答錯!!". "</div>";
            }
        }else{
            echo "<div style=\"color:blue\">" ."未作答!!". "</div>";
            echo "正確答案為 " .$papertmp[$k][$q][$a]. "<br />";
        }
    }

    public function ans_for($answertmp, $papertmp, $k, $q){
        for ($a=0; $a < count($papertmp[$k][$q]); $a++){
            global $student_anstmp;
            if (isset($answertmp[$k][$q][$a])){
                echo substr($answertmp[$k][$q][$a], 1) == ""?"<div style=\"color:blue\">" ."未作答!!". "</div>":"你的答案為 ".substr($answertmp[$k][$q][$a], 1)."<br />";
                echo "正確答案為 " .$papertmp[$k][$q][$a]. "<br />";
                if (!strcasecmp(substr($answertmp[$k][$q][$a], 1), $papertmp[$k][$q][$a])){
                    echo "<div style=\"color:green\">" ."答對!!". "</div>";
                }else{
                    echo substr($answertmp[$k][$q][$a], 1) == ""?"":"<div style=\"color:red\">" ."答錯!!". "</div>";
                }
            }/*else{
                echo "<div style=\"color:blue\">" ."未作答!!". "</div>";
                echo "正確答案為 " .$papertmp[$k][$q][$a]. "<br />";
            }*/
        }
    }
    
    public function nonans_for($papertmp, $k, $q){
        for ($a=0; $a < count($papertmp[$k][$q]); $a++){
            echo "<div style=\"color:blue\">" ."未作答!!". "</div>";
            echo "正確答案為 " .$papertmp[$k][$q][$a]. "<br />";
        }
    }
    
    public function list_options($r_q_opt_ans, $check_choose, $q_num, $r_q_opt_ans_desc, $part_num){//單複選函數
		for ($i=0; $i < count($r_q_opt_ans); $i++){
            $opt_ans = $r_q_opt_ans[$i];
			if($check_choose>1){
				echo "<input style=\"WIDTH: 15px; HEIGHT: 15px\" type=\"checkbox\" name=\"$part_num-$q_num\" value=D$opt_ans>";
				}else{
					echo "<input style=\"WIDTH: 15px; HEIGHT: 15px\" type=\"radio\" name=\"$part_num-$q_num\" value=A$opt_ans>";
				}
			echo $r_q_opt_ans[$i];
			echo ". ";
			echo "<label for=\"$q_num\">" .$r_q_opt_ans_desc[$i]. "</label><br />";
			}
		}

    //題目放入圖片
	public function pics_in_topic($pics,$rootpath,$q_num,$topic_filter){
		$count_pics=count($pics);
		
		for($p=0;$p < $count_pics;$p++){
			$filepath = $rootpath. "capture/img".$pics[$p];
			$topic=str_replace("{{pic}}","<br><img src='$filepath'><br>",$topic_filter);//過濾取代成特定tag
		}echo "<h3>". $q_num . ". " .$topic. "</h3><br>";
	}
    
    public function studentanswer($student_anstmp, $answertmp, $papertmp){ 
        $qq = 0;
        for ($k=0; $k < count($papertmp); $k++){
            for ($q=0; $q < count($papertmp[$k]); $q++){
                if (isset($papertmp[$k][$q])){
                    for ($a=0; $a < count($papertmp[$k][$q]); $a++){
                        if (!isset($answertmp[$k][$q][$a])){
                            $student_anstmp[$qq][] = "";
                        }else{
                            $student_anstmp[$qq][] = substr($answertmp[$k][$q][$a], 1) == ""?"":substr($answertmp[$k][$q][$a], 1);
                        }
                    }
                }
                $qq++;
            }
        }
        return $student_anstmp;
    }
    
    public function rightanswer($right_anstmp, $papertmp){
        $qq = 0;
        for ($k=0; $k < count($papertmp); $k++){
            for ($q=0; $q < count($papertmp[$k]); $q++){
                if (isset($papertmp[$k][$q])){
                    for ($a=0; $a < count($papertmp[$k][$q]); $a++){
                        $right_anstmp[$qq][] = $papertmp[$k][$q][$a];
                    }
                }
                $qq++;
            }
        }
        return $right_anstmp;
    }
    
    public function mysqlqid($mysqlqid_tmp, $row){

        for ($k=0; $k < count($row); $k++){
            for ($q=0; $q < count($row[$k][0]->questions); $q++){
                $mysqlqid_tmp[] = $row[$k][0]->questions[$q]->mysql_q_id;
            }
        }
        return $mysqlqid_tmp;
    }
    
}
