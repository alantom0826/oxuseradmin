<?php
require_once(Yii::getPathOfAlias('application.extensions.smarty.libs').DIRECTORY_SEPARATOR.'Smarty.class.php');
define('SMARTY_VIEW_DIR', Yii::getPathOfAlias('application.views.smarty')); 

class CSmarty extends Smarty
{
    const DIR_SEP = DIRECTORY_SEPARATOR;
    function __construct()
    {
        parent::__construct();
        $this->template_dir = SMARTY_VIEW_DIR;  
        $this->compile_dir = SMARTY_VIEW_DIR.self::DIR_SEP.'tpl_c';  
        $this->caching = true;  
        $this->cache_dir = SMARTY_VIEW_DIR.self::DIR_SEP.'cache';  
        $this->left_delimiter  =  '{';  
        $this->right_delimiter =  '}';  
        $this->cache_lifetime = 3600;
        
        
        //~ $this->_smarty = new Smarty();
        //~ $this->_smarty->template_dir = SMARTY_VIEW_DIR.DS.'tpl';
        //~ $this->_smarty->compile_dir = SMARTY_VIEW_DIR.DS.'tpl_c';
        //~ $this->left_delimiter =  '<!--{';
        //~ $this->right_delimiter = '}-->';
    }

    function init(){
        Yii::registerAutoloader('smartyAutoload');
    }
}
?>
