<?php
/* @var $this SetupController */

$this->breadcrumbs=array(
	'Setup',
);
?>
<script>
function save(){
    alert("儲存成功!!");
}
</script>
<h1><?php //echo $this->id . '/' . $this->action->id; ?></h1>
<h1>設定</h1>
<?php
    $data = file_get_contents("php://input");
    $data = urldecode($data);
    if (!empty($data)){
        $data = explode("&", $data);
        for ($i=0; $i < count($data); $i++){
            $data[$i] = explode("=", $data[$i]);
        }
        $confvalue = [
                'projectname' => $data[0][1],
                'version' => $data[1][1],
                'resultaddress'=>$data[2][1],
                'DBname'=>$data[3][1],
                'username'=>$data[4][1],
                'password'=>$data[5][1],
                'rootpath'=>$data[6][1],
                'picfilepath'=>$data[7][1],
                'audiofilepath'=>$data[8][1],
            ];
        //print_r($confvalue);
        $confvalue = json_encode($confvalue);
        $fp = fopen($_SERVER['DOCUMENT_ROOT']. "/oxuseradmin/oxexam.conf","w");
        fwrite($fp,$confvalue);
        fclose($fp);
    }
?>

<?php
    $fp = fopen($_SERVER['DOCUMENT_ROOT']. "/oxexam_online/oxexam.conf","r");
    $json = fread($fp,filesize($_SERVER['DOCUMENT_ROOT']. "/oxexam_online/oxexam.conf"));
    //~ $file = json_decode(urldecode($json));
    $file = json_decode($json);
?>

<form name="sform" method="post">
    <div class="row">
        <div class="large-6 columns">
            <h3>一般 設定</h3>
            Projectname: <input type="text" name="Projectname" value="<?php echo $file->projectname; ?>"><br>
            Version: <input type="text" name="Version" value="<?php echo $file->version; ?>"><br>
            resultaddress: <input type="text" name="resultaddress" value="<?php echo $file->resultaddress; ?>"><br>
            <h3>MySql 設定</h3>
            DBname: <input type="text" name="DBname" value="<?php echo $file->DBname; ?>"><br>
            username: <input type="text" name="username" value="<?php echo $file->username; ?>"><br>
            password: <input type="text" name="password" value="<?php echo $file->password; ?>"><br>
            <h3>圖檔,音訊,影像檔路徑 設定</h3>
            rootpath: <input type="text" name="rootpath" value="<?php echo $file->rootpath; ?>"><br>
            picfilepath: <input type="text" name="picfilepath" value="<?php echo $file->picfilepath; ?>"><br>
            audiofilepath: <input type="text" name="audiofilepath" value="<?php echo $file->audiofilepath; ?>"><br>
        </div>
    </div>
    
    <div class="row">
        <div class="large-6 columns">
            <h3>語言 設定</h3>
            <?php
                foreach (Yii::app()->UrlManager->listLanguage() as $language => $languageUrl) {
                echo '<ul>';
                if (Yii::app()->language==$language) {
                    echo '<li>'.CHtml::label(Yii::app()->getModule('oxexam')->lang($language), $language, array('style'=>'color:#f06d32;font-size:100%;')).'</li>';
                }else{
                    echo '<li>'.CHtml::link(Yii::app()->getModule('oxexam')->lang($language),$languageUrl).'</li>';
                }
                echo '</ul>';
                }
            ?>
            <input id="submit" class="button" type="submit" value="儲存" onclick="save()">
        </div>
    </div>

</form>

<!--
<p>
	You may change the content of this page by modifying
	the file <tt><?php //echo __FILE__; ?></tt>.
</p>
-->
