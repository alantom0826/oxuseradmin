<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    

<!--
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation5.2.x.min.css" />
-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/oxuseradmin.css" />


    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/modernizr.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

<style type="text/css">
top-bar-title a {color: #ffffff;font-size: 120%;}
@font-face{
    font-family : PWChalk;
    src : url("<?php echo Yii::app()->request->baseUrl; ?>/fonts/PWChalk.ttf");
}
fontPW { font-family : PWChalk;}

@font-face{
    font-family : odohei;
    src : url("<?php echo Yii::app()->request->baseUrl; ?>/fonts/odohei.ttf");
}
<!--
body { font-family: odohei; }
-->

</style>

</head>

<body>
<?php
    $this->widget(
        'booster.widgets.TbNavbar',
        array(
            'type' => 'inverse',
            'brand' => 'OxUserAdmin',
            'brandUrl' => '#',
            'collapse' => true, // requires bootstrap-responsive.css
            'fixed' => false,
            'fluid' => true,
            'items' => array(
                array(
                    'class' => 'booster.widgets.TbMenu',
                    'type' => 'navbar',
                    'items' => array(
                        array('label' => 'Home', 'url' => '#', 'active' => true),
                        array('label' => "Ver ".Yii::app()->getModule('oxexam')->loadconf('version'), 'url' => '#'),
                    ),
                ),
                //'<form class="navbar-form navbar-left" action=""><div class="form-group"><input type="text" class="form-control" placeholder="Search"></div></form>',
                array(
                    'class' => 'booster.widgets.TbMenu',
                    'type' => 'navbar',
                    'htmlOptions' => array('class' => 'pull-right'),
                    'items' => array(
                        array('url'=>Yii::app()->getModule('user')->loginUrl, 'label'=>Yii::app()->getModule('user')->t("Login"), 'visible'=>Yii::app()->user->isGuest),
                        array('url'=>array('/setup/setup'), 'label' => Yii::app()->getModule('user')->t('Setup'), 'visible'=>!Yii::app()->user->isGuest && UserModule::isAdmin()),
                        array('url'=>Yii::app()->getModule('user')->oxuseradminindexUrl, 'label'=>Yii::app()->getModule('user')->t("UserList"), 'visible'=>!Yii::app()->user->isGuest && UserModule::isAdmin()),
                        array('url'=>Yii::app()->getModule('user')->logoutUrl, 'label'=>Yii::app()->getModule('user')->t("Logout").' ('.Yii::app()->user->name.')', 'visible'=>!Yii::app()->user->isGuest),
                        '---',
                        array(
                            'label' => Yii::app()->getModule('user')->t("Language"),
                            'url' => '#',
                            'items' => array(
                                array('label' => '英文', 'url' => '/oxuseradmin/en_us/user/login'),
                                array('label' => '繁體中文', 'url' => '/oxuseradmin/zh_tw/user/login'),
                                array('label' => '簡體中文', 'url' => '/oxuseradmin/zh_cn/user/login'),
                            ),
                            'visible'=>Yii::app()->user->isGuest,
                        ),
                    ),
                ),
            ),
        )
    );
?>


	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div><!-- clear -->

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by OSS Integral Institute Co., Ltd.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- ng-app='TimeoutModal' -->

<!--
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/fastclick.js"></script>
-->

<script> $(document).foundation(); </script>
</body>
</html>
