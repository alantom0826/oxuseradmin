<?php
    $fp = fopen($_SERVER['DOCUMENT_ROOT']. "/oxexam_online/oxexam.conf","r");
    $json = fread($fp,filesize($_SERVER['DOCUMENT_ROOT']. "/oxexam_online/oxexam.conf"));
    $file = json_decode($json);


// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'OxUserAdmin',

    // preloading 'log' component
    'preload'=>array(
        'log',
        'bootstrap'
    ),

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.extensions.smarty.sysplugins.*',
    ),
    
    // YiiBooster includes all the features from its parent
    // project Yii-Bootstrap, thus its gii templates
    'modules'=>array(
        'user'=>array(),
        // uncomment the following to enable the Gii tool
        
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'1234',
             // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1', '192.168.1.*'),
            'generatorPaths' => array(
                'bootstrap.gii'
            ),
        ),
        'oxexam'=>array(
        ),

    ),

    // application components
    'components'=>array(
        // login module
        'user'=>array(
            // enable cookie-based authentication
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
            
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Booster',
            'responsiveCss' => true,
        ),
        // smarty templates engine
        'smarty'=>array(
            'class'=>'application.extensions.CSmarty',
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager'=>array(
            'class'=>'ext.yii-multilanguage.MLUrlManager',
            'urlFormat'=>'path',
            'languages'=>array(
            'en_us',
            'zh_tw',
            'zh_cn',
            ),
            'showScriptName'=>false,
            //~ 'rules'=>array(
                //~ '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                //~ '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                //~ '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            //~ ),
            'rules'=>array(
            '<controller:\w+>/<id:\d+>'=>'<controller>/view',
            '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            '<module:\w+>/<controller:\w+>/<id:\d+>/<action:\w+>'=>'<module>/<controller>/<action>',
            '<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
            '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
            '<module:\w+>/<controller:\w+>'=>'<module>/<controller>',
            '<module:\w+>'=>'<module>',
            ),
        ),
        // uncomment the following to use a SQLite database
        /*
        'db'=>array(
            'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
        ),
        */
        // uncomment the following to use a MySQL database
        'db'=>array(
			'connectionString' => "mysql:host=localhost;dbname=$file->DBname",
			'emulatePrepare' => true,
			'username' => "$file->username",
			'password' => "$file->password",
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
        ),
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'webmaster@example.com',
    ),
);
