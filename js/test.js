app = angular.module('myApp', ['mm.foundation','ngSanitize']);
app.factory('alertService', function($rootScope) {
    var alertService = {};
 
    // 创建一个全局的 alert 数组
    $rootScope.alerts = [];
 
    alertService.add = function(type, msg) {
        $rootScope.alerts.push({'type': type, 'msg': msg, 'close': function(){ alertService.closeAlert(this); }});
    };
 
    alertService.closeAlert = function(alert) {
        alertService.closeAlertIdx($rootScope.alerts.indexOf(alert));
    };
 
    alertService.closeAlertIdx = function(index) {
        $rootScope.alerts.splice(index, 1);
    };
 
    return alertService;
});

app.controller('UserCtrl', ['$scope', 'alertService',
    function($scope, alertService) {
        //~ alertService.add('warning', '这是一个警告消息！');
        //~ alertService.add('error', "这是一个出错消息！");
 
        $scope.editProfile = function() {
            alertService.add('success', '<h4>成功！</h4> 你的个人资料已经修改。');
            alertService.add('error', "这是一个出错消息！");
            alertService.add('warning', '这是一个警告消息！');
        };
        
        $scope.alertbox = function(event, content){
            switch(event){
                case 1:
                    alertService.add('success', '<h4>成功！</h4> 你的个人资料已经修改。');
                    break;
                case 2:
                    alertService.add('error', "这是一个出错消息！");
                    break;
                case 3:
                    alertService.add('warning', '这是一个警告消息！');
                    break;
            }
        };
    }
]);

app.controller('salertCtrl', ['$scope', 'alertService',
    function($scope, alertService) {
        $scope.alertboxs = function(content){
            alertService.add('success', content);
        };
    }
]);

app.controller('ealertCtrl', ['$scope', 'alertService',
    function($scope, alertService) {
        $scope.alertboxs = function(content){
            alertService.add('error', content);
        };
    }
]);

app.controller('walertCtrl', ['$scope', 'alertService',
    function($scope, alertService) {
        $scope.alertboxs = function(content){
            alertService.add('warning', content);
        };
    }
]);
