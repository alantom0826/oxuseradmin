
lock = function(theEvent) {
    if (theEvent != null) { event = theEvent; }
    // 擋滑鼠右鍵選單事件
    if (event.type == "contextmenu") {
        return false;
    // 擋滑鼠中鍵與右鍵事件
    } else if (event.type == "mousedown") {
        if (event.button == 2 || event.button == 3) { return false; }
    // 擋 IE 按下 F1 鈕時會觸發的 onhelp 事件
    } else if (event.type == "help") {
        return false;
    // 擋特定按鍵
    } else if (event.type == "keydown") {
        // 擋 alt、ctrl 鍵
        if (event.altKey || event.ctrlKey || event.shiftkey) { return false; }
        // 擋 F1 ~ F12 功能鍵，其中此種寫法只能擋 FF 的 F1 鍵，無法擋 IE 的 F1 鍵
        if (event.keyCode >= 112 && event.keyCode <= 123) {
            try {
                // IE 要將 keyCode 設為 0 才能真正擋下 F2 ~ F12 功能鍵，
                // 但 FF 會丟 Exception，所以用 try-cache 擋住
                event.keyCode = 0; 
            } catch(e){}
            return false;
        }
    }
}

document.onmousedown = lock;
document.oncontextmenu = lock;
document.onkeydown = lock;
window.onhelp = lock;

app = angular.module('TimeoutModal', ['mm.foundation', 'ngSanitize']);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});

app.directive('modalDialog', function() {
    return {
        restrict: 'E',
        scope: {
            show: '='
        },
        replace: true, // Replace with the template below
        transclude: true, // we want to insert custom content inside the directive
        link: function($scope, $element, $attrs) {
            $scope.dialogStyle = {};
            if ($attrs.width)
                $scope.dialogStyle.width = $attrs.width;
            if ($attrs.height)
                $scope.dialogStyle.height = $attrs.height;
            $scope.hideModal = function() {
                $scope.show = false;
            };
        },
        template:"<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle' ng-click='hideModal()'><div class='ng-modal-close' ng-click='hideModal()'>II</div><div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
    };
});


app.controller('TimeCtrl', ['$scope', 'alertService', function($scope, alertService) {

window.onbeforeunload = function(){
    return '請勿重載網頁～成績將以0分計算!!';
    //document.forms["eform"].submit();
}

    $scope.starttime = new Date().getTime()/1000.0;
    $scope.pausetime = 0;
    $scope.checkclr = false;
    $scope.findex = 1;
    $scope.fcount = true;
    var timer;
    
    $scope.start = function(duration, check_return){
        $scope.modalShown = false;
        if (check_return){
            window.onbeforeunload = null;
            document.forms["eform"].submit();
        }
        $scope.endtime = Math.floor($scope.starttime) + duration * 60;
        if ($scope.starttime >= $scope.endtime){
            alertService.add('warning', '已經超過考試時間！');
        }else{
            timer = setInterval(function(){
            if ($scope.pausetime !== 0){
                $scope.endtime = Math.floor(new Date().getTime()/1000.0) + $scope.pausetime - 1;
                $scope.pausetime = 0;
            }
            $scope.real_time = $scope.endtime - Math.floor(new Date().getTime()/1000.0);
            if ($scope.real_time <= 0){
                alertService.add('warning', '考試結束！');
                $scope.clear();
                $scope.real_time = 0.0;
                window.onbeforeunload = null;
                $scope.ngsubmit('esubmit');
            }
            $scope.seconds = Math.floor($scope.real_time  % 60);
            $scope.seconds = ($scope.seconds < 10 ? "0" : "" ) + $scope.seconds;
            $scope.minutes = Math.floor((($scope.real_time / (60)) % 60));
            $scope.minutes = ($scope.minutes < 10 ? "0" : "" ) + $scope.minutes;
            $scope.hours = Math.floor((($scope.real_time / (3600)) % 24));
            $scope.hours = ($scope.hours < 10 ? "0" : "" ) + $scope.hours;
            $scope.days = Math.floor((($scope.real_time / (3600)) / 24));
            $scope.days = ($scope.days < 10 ? "0" : "" ) + $scope.days;
            $scope.$apply(); 
            }, 1000);
        }
    };
 
    $scope.stop = function(){
        window.clearInterval(timer);
    };
 
    $scope.pause = function(){
        $scope.modalShown = false;
        window.clearInterval(timer);
        $scope.pausetime = $scope.real_time;
        $scope.modalShown = !$scope.modalShown;
        $scope.checkonce = 1;
    };
  
    // clear time to 0
    $scope.clear = function(){
        $scope.stop();
        $scope.checkclr = !$scope.checkclr;
    };
  
    $scope.disreloadbox = function(){
        window.onbeforeunload = null;
    };
    
    $scope.reloadbox = function(){
        window.onbeforeunload = function(e){ return "是否要離開此頁面??"; }
    };
    
    $scope.ngsubmit = function(name){
        document.getElementById(name).click();
    };
    
    $scope.q_max = function(q_maxs){
        if ($scope.fcount){ 
            $scope.bindex = q_maxs + 1;
            $scope.fcount = !$scope.fcount;
        }
    };
    
    $scope.forward_q = function(q_max){
        $scope.findex++;
        $scope.bindex = $scope.findex;
        if ($scope.findex == q_max)
            $scope.findex = 0;
        if ($scope.bindex == 1)
            $scope.bindex = q_max + 1;    
    };
    
    $scope.backward_q = function(q_max){
        $scope.bindex--;
        $scope.findex = $scope.bindex;
        if ($scope.bindex == 1)
            $scope.bindex = q_max + 1;
        if ($scope.findex == q_max)
            $scope.findex = 0;
    };
    
    $scope.choice_q = function(q_index, q_max){
        $scope.bindex = $scope.findex = q_index;
        if (q_index == q_max)
            return $scope.findex = 0;
        if (q_index == 1)
            return $scope.bindex = q_max + 1;

    };
    
    $scope.test01 = function(){
        alertService.add('warning', '已經超過考試時間！');
    }

}]);

app.factory('alertService', function($rootScope) {
    var alertService = {};
 
    // 創建一個全局的 alert Array
    $rootScope.alerts = [];
 
    alertService.add = function(type, msg) {
      $rootScope.alerts.push({'type': type, 'msg': msg, 'close': function(){ alertService.closeAlert(this); }});
    };
 
    alertService.closeAlert = function(alert) {
      alertService.closeAlertIdx($rootScope.alerts.indexOf(alert));
    };
 
    alertService.closeAlertIdx = function(index) {
      $rootScope.alerts.splice(index, 1);
    };
 
    return alertService;
});
